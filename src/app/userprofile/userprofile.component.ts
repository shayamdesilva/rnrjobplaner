import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  dataSource ;
  
  displayedColumns: string[] = ['userId','name', 'enable', 'sysAdmin', 'salesModule','jobCosting','jobCostSales','jobCostGP','jobCostCost','id'];
  PeriodicElement:any=[];
  PeriodicElement1:any=[];
  ELEMENT_DATA//: PeriodicElement[] //= [
  userStaffArray:any=[];
  logUserId:string;

  constructor(private http:HttpClient) { 
    this.dataSource = this.ELEMENT_DATA;
    this.logUserId = localStorage.getItem('tokenid'); 

  }

  ngOnInit(): void {


    this.PeriodicElement.length = 0;
    this.getAllUsers().subscribe(data => {
       for (var i = 0; i < data.length; i++){
         if(this.logUserId === data[i]['id']){
          this.PeriodicElement.push({
            userId:data[i]['userId'],
            name:data[i]['name'],
            enable:data[i]['enable'],
            sysAdmin: data[i]['sysAdmin'],
            salesModule: data[i]['sysAdmin'],
            jobCosting: data[i]['jobCosting'],
            jobCostSales: data[i]['jobCostSales'],
            jobCostGP: data[i]['jobCostGP'],
            jobCostCost:data[i]['jobCostCost'],
            id:data[i]['id'],
  
  
  
  
           })

         }

         this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
         this.dataSource.sort = this.sort;
         this.dataSource.paginator = this.paginator;
         }
      });
  }


  getAllUsers():Observable<String[]>{
    return this.http.get<String[]>('http://localhost:50388/api/staff');
  }

  applyFilter(filterValue:string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  SaveAdminstrationData(){
    console.log(this.dataSource.filteredData)
    for(var x =0;x<this.dataSource.filteredData.length;x++ ){
      console.log(this.dataSource.filteredData[x]['id']);
      this.userStaffArray.push({
        userId:this.dataSource.filteredData[x]['userId'],
        name:this.dataSource.filteredData[x]['name'],
        enable:this.dataSource.filteredData[x]['enable'],
        sysAdmin:this.dataSource.filteredData[x]['sysAdmin'],
        salesModule:this.dataSource.filteredData[x]['salesModule'],
        jobCosting:this.dataSource.filteredData[x]['jobCosting'],
        jobCostSales:this.dataSource.filteredData[x]['jobCostSales'],
        jobCostGP:this.dataSource.filteredData[x]['jobCostGP'],
        jobCostCost:this.dataSource.filteredData[x]['jobCostCost'],
        id:this.dataSource.filteredData[x]['id'],


      });
      var url = 'http://localhost:50388/api/staff/addprodstaff';
      const body=JSON.stringify(this.userStaffArray);
      let headers = new Headers({ 'Content-Type': 'application/json' });
    this.http.post(url, this.userStaffArray).subscribe(res => console.log(res));


    }
  }

}
