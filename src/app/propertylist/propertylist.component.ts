import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-propertylist',
  templateUrl: './propertylist.component.html',
  styleUrls: ['./propertylist.component.css']
})
export class PropertylistComponent implements OnInit {

  properties: Array<any> =[
    {
        "Id":1,
        "Type":"Home",
        "Price":"Rs.1200.00",
        "Name":"Single Home"
    },
    {
        "Id":2,
        "Type":"Home1",
        "Price":"Rs.1500.00",
        "Name":"Double Level Home"
  },
  {
        "Id":3,
        "Type":"Home3",
        "Price":"Rs.1300.00",
        "Name":"Three Level Home"
  },
  {
      "Id":4,
      "Type":"Home4",
      "Price":"Rs.1400.00",
      "Name":"Fore Level Home"
  },
  {
      "Id":5,
      "Type":"Home5",
      "Price":"Rs.1500.00",
      "Name":"Five Level Home"
  },
  {
      "Id":6,
      "Type":"Home6",
      "Price":"Rs.1600.00",
      "Name":"Six Level Home"
  }
]

  constructor() { }

  ngOnInit(): void {
  }

}
