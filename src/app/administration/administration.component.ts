import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

// const ELEMENT_DATA: PeriodicElement[] = [
//   {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
//   {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
//   {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
//   {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
//   {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
//   {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
//   {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
// ];

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})
export class AdministrationComponent implements OnInit {

  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  dataSource ;
  
  displayedColumns: string[] = ['stockCode','opCode', 'assignTo', 'estimatedTime', 'status', 'orderQty' ,'startDate','productionQty','scrapQTY','completeDate','orderDate','dueDate','inStage','drawing','division'];
  PeriodicElement:any=[];
  PeriodicElement1:any=[];
  isManufacture:boolean;
  ELEMENT_DATA//: PeriodicElement[] //= [
  userStaffArray:any=[];
  constructor(private http:HttpClient) { 
    this.dataSource = this.ELEMENT_DATA;
    this.isManufacture = false;

  }

  ngOnInit(): void {
    this.getAllStaffUsers().subscribe(data => {
      for (var i = 0; i < data.length; i++){
        if(data[i]['name'] === localStorage.getItem('tokenname')){
          if(data[i]['manufacture']){
            console.log('manufacture : '+ data[i]['name']);

            this.PeriodicElement.length = 0;
            this.getAllUsers().subscribe(data => {
               for (var i = 0; i < data.length; i++){
                    if(data[i]['assignTo'] === localStorage.getItem('tokenname')){
                      this.PeriodicElement.push({
                        stockCode:data[i]['stockCode'],
                        opCode:data[i]['opCode'],
                        assignTo:data[i]['assignTo'],
                        estimatedTime: data[i]['estimatedTime'],
                        status: data[i]['status'],
                        orderQty:data[i]['orderQty'],
                        startDate: data[i]['startDate'],
                        productionQty: data[i]['productionQty'],
                        scrapQTY: data[i]['scrapQTY'],
                        completeDate:data[i]['completeDate'],
                        orderDate:data[i]['orderDate'],
                        dueDate:data[i]['dueDate'],
                        inStage:data[i]['inStage'],
                        drawing:data[i]['drawing'],
                        division:data[i]['division'],
              
                       })
                    }


                 this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
                 this.dataSource.sort = this.sort;
                 this.dataSource.paginator = this.paginator;
                 }
              });




          }else{
            this.PeriodicElement.length = 0;
            this.getAllUsers().subscribe(data => {
               for (var i = 0; i < data.length; i++){
                 this.PeriodicElement.push({
                  stockCode:data[i]['stockCode'],
                  opCode:data[i]['opCode'],
                  assignTo:data[i]['assignTo'],
                  estimatedTime: data[i]['estimatedTime'],
                  status: data[i]['status'],
                  orderQty:data[i]['orderQty'],
                  startDate: data[i]['startDate'],
                  productionQty: data[i]['productionQty'],
                  scrapQTY: data[i]['scrapQTY'],
                  completeDate:data[i]['completeDate'],
                  orderDate:data[i]['orderDate'],
                  dueDate:data[i]['dueDate'],
                  inStage:data[i]['inStage'],
                  drawing:data[i]['drawing'],
                  division:data[i]['division'],
        
                 })
                 this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
                 this.dataSource.sort = this.sort;
                 this.dataSource.paginator = this.paginator;
                 }
              });
          }
          

        }
      }
      
    });


   
  }

  getAllUsers():Observable<String[]>{
    return this.http.get<String[]>('http://localhost:50388/api/admin');
  }

  getAllStaffUsers():Observable<String[]>{
    return this.http.get<String[]>('http://localhost:50388/api/staff');
  }

  applyFilter(filterValue:string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  SaveAdminstrationData(){
    // console.log(this.dataSource.filteredData)
    // for(var x =0;x<this.dataSource.filteredData.length;x++ ){
    //   console.log(this.dataSource.filteredData[x]['id']);
    //   this.userStaffArray.push({
    //     userId:this.dataSource.filteredData[x]['userId'],
    //     name:this.dataSource.filteredData[x]['name'],
    //     enable:this.dataSource.filteredData[x]['enable'],
    //     sysAdmin:this.dataSource.filteredData[x]['sysAdmin'],
    //     salesModule:this.dataSource.filteredData[x]['salesModule'],
    //     jobCosting:this.dataSource.filteredData[x]['jobCosting'],
    //     jobCostSales:this.dataSource.filteredData[x]['jobCostSales'],
    //     jobCostGP:this.dataSource.filteredData[x]['jobCostGP'],
    //     jobCostCost:this.dataSource.filteredData[x]['jobCostCost'],
    //     id:this.dataSource.filteredData[x]['id'],


    //   });
    //   var url = 'http://localhost:50388/api/staff/addprodstaff';
    //   const body=JSON.stringify(this.userStaffArray);
    //   let headers = new Headers({ 'Content-Type': 'application/json' });
    // this.http.post(url, this.userStaffArray).subscribe(res => console.log(res));


    // }
  }

}
