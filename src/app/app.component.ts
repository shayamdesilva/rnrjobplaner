import { Component } from '@angular/core';
import { Config, Menu } from './accordion/types';

@Component({
  selector: 'app-root',
  template: `
    <h1>Accordion Menu</h1>
    <accordion 
      [options]="options" 
      [menus]="menus">
    </accordion>
  `,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  
  
})
export class AppComponent {
  title = 'myob_exo_business';


  
}
