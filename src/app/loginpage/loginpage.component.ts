import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { AuthUserService } from 'src/services/auth-user.service';
import { Router } from '@angular/router';
 import * as alertify from 'alertifyjs'


@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
@ViewChild('Form') addPropertyForm: NgForm

userLoginData:any =[];
  constructor(private http:HttpClient,
    private authservices:AuthUserService,
    private router:Router
    ) { }

  ngOnInit(): void {
    this.getAllUser().subscribe(data => {
    this.userLoginData = data;
    var isValidCredintials = true;
    });
  }

  //Submit button clicked
  onSubmit(Form :NgForm){
    //Call api and iterate the response
    console.log('Login : '+this.addPropertyForm.value.password);
    this.getAllUser().subscribe(data => {
    this.userLoginData = data;
    var isValidCredintials = true;
    for (var i = 0; i < data.length; i++){
         var password = data[i]['password'];
        if(data[i]['password'] === this.addPropertyForm.value.password &&
            data[i]['userId'] ===this.addPropertyForm.value.username){
            isValidCredintials = true;
            const token = this.authservices.authUser(this.addPropertyForm.value);
            localStorage.setItem('token',this.addPropertyForm.value.username);
            localStorage.setItem('tokenid',data[i]['id']);
            localStorage.setItem('tokenname',data[i]['name']);
            console.log('Name '+data[i]['name']);
            console.log('ID '+data[i]['id']);
            this.router.navigate(['/home']);
            break;
        }else{
          isValidCredintials = false;
        }
      }
      if(!isValidCredintials){
         alertify.error("Please enter a valid user name and password");
      }else{
      }
    });
  }

  //Call to Api and get data.
  getAllUser():Observable<String[]>{
    this.userLoginData = this.http.get<String[]>('http://localhost:50388/api/staff');
    return this.userLoginData;
  }
}
