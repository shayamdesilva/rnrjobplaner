import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as alertyfy from 'alertifyjs';

@Component({
  selector: 'app-userregister',
  templateUrl: './userregister.component.html',
  styleUrls: ['./userregister.component.css']
})
export class UserregisterComponent implements OnInit {
  @ViewChild('Form') userRegisterForm: NgForm
  user:any ={}

  registerationForm: FormGroup
  constructor(private router:Router) { }

  ngOnInit(): void {
    this.registerationForm = new FormGroup({
       userName:new FormControl(null,Validators.required),
       email:new FormControl(null,[Validators.required,Validators.email]),
       password:new FormControl(null,[Validators.required,Validators.minLength(8)]),
       confirmpassword:new FormControl(null,[Validators.required]),
       mobile:new FormControl(null,[Validators.required,Validators.maxLength(10)])
    });
  }

  // passwordMatchingValidator(fg:FormGroup):Validators{
  //   return fg.get('password').value === fg.get('confirmPassword').value ? null : {
  //     notmatched:true
  //   };
  // }

  onSubmit(Form :NgForm){
    if(this.userRegisterForm.valid){
      this.user = Object.assign(this.user,this.userRegisterForm.value);
      this.addUser(this.user);
      this.registerationForm.reset();

    }
    this.router.navigate(['/home']);


  }

  addUser(user){
    let users =[];
    if(localStorage.getItem('Users')){
      users = JSON.parse(localStorage.getItem('Users'));
      users =[user,...users];

    }else{
      users = [user];
    }
    localStorage.setItem('Users',JSON.stringify(users));
  }

}
