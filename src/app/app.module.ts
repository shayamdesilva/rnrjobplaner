import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { PropertyCardComponent } from './property/property-card/proprty-card.components';
import { NavbarComponent } from './navbar/navbar.component';
import { PropertylistComponent } from './propertylist/propertylist.component';

import { LoginpageComponent } from './loginpage/loginpage.component';
import { AddPropertyComponent } from './property/add-property/add-property.component';
import { HomeComponent } from './home/home.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';;
import { TableComponent } from './table/table.component';
import { AuthUserService } from 'src/services/auth-user.service';
import { UserregisterComponent } from './userregister/userregister.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';
import {MatMenuModule} from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SalesOrderComponent } from './sales-order/sales-order.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {BreadcrumbModule} from 'angular-crumbs';




import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import { AdministrationComponent } from './administration/administration.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { CardprofileComponent } from './cardprofile/cardprofile.component';





const appRoutes:Routes=[
{path:'', component:LoginpageComponent},
{path:'home', component:HomeComponent},
{path:'link', component:HomeComponent},
{path:'sell', component:HomeComponent}, 
{path:'disa/:id', component:AddPropertyComponent},
{path:'user/register',component:UserLoginComponent},
{path:'user/login', component:LoginpageComponent},
{path:'salesorder', component:SalesOrderComponent},
{path:'dashboard1', component:DashboardComponent},
{path:'Setting/Administration', component:CardprofileComponent},
{path:'myprofile', component:UserprofileComponent},
{path:'dashboard', component:AdministrationComponent},
{path:'cardprofile', component:CardprofileComponent},

]

@NgModule({
  declarations: [
    AppComponent,
    PropertyCardComponent,
    NavbarComponent,
    PropertylistComponent,
    LeftMenuComponent,
    LoginpageComponent,
    AddPropertyComponent,
    HomeComponent,
    TableComponent,
    UserregisterComponent,
    UserLoginComponent,
    LeftMenuComponent,
    DashboardComponent,
    SalesOrderComponent,
    AdministrationComponent,
    UserprofileComponent,
    CardprofileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    NgbModule,
    MatMenuModule,
    MatSidenavModule,
    MatButtonModule,
    MatListModule,
    TabsModule.forRoot(),
    BrowserAnimationsModule,
    MatExpansionModule,
    MatSortModule,
    MatTableModule,
    BreadcrumbModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ],
  providers: [
    AuthUserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
