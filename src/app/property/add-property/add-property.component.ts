import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-property',
  templateUrl: './add-property.component.html',
  styleUrls: ['./add-property.component.scss']
})
export class AddPropertyComponent implements OnInit {

  public propertyid : number;
  constructor(private route:ActivatedRoute) { }

  ngOnInit() {
    this.propertyid = this.route.snapshot.params['id']
  }



}
