import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector:'app-property-card',
 // template:'<h1>I am a card</h1>',
 templateUrl:'property-card.component.html',
 // styles: ['h1{font:weight:normal}'
styleUrls: ['property-card.component.css']
})
export class PropertyCardComponent{
@Input() property:any
  /*Property: any ={
    "Id":1,
    "Type":"Home",
    "Price":"Rs.1200.00",
    "Name":"Two Level Home"
  }*/

  constructor(private http:HttpClient) { }
  cityList:String[];

  ngOnInit(): void {
    this.getAllCity().subscribe(data =>{
      this.cityList = data;
      console.log('Value '+data)
    })
  }

  getAllCity(): Observable<String[]>{
    return this.http.get<String[]>('http://localhost:5000/api/user');

  }

}
