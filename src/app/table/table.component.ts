
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import * as alertify from 'alertifyjs'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import { jsPDF } from "jspdf";
import { trim } from 'jquery';
import { Guid } from "guid-typescript";


export interface PeriodicElement {
  jobcard: string;
  salesid: number;
  stockcode: string;
  status: string;
  instage: string;
  orderdate:Date;
  DueDate:Date
  CompleteDate:Date
  OrderQTY:Date
  ProductionStartDate:Date
  ProductionComplete:Date
  Sales:number
  Cost:number
  Gp:number
  Note:string
  AddtionalNotes:string
  Color:string




}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})

export class TableComponent implements OnInit {
  @ViewChild('Form') addcompo:NgForm
  @ViewChild(MatSort) sort:MatSort;
  @ViewChild(MatPaginator) paginator:MatPaginator;
  displayedColumns: string[] ;
  dataSource ;
  logo:string='assets/images/logo.png'
 
  name = 'Angular';
  jobCostingArray:any=[];
  panelOpenState = false;
  userData:any =[];
  addCards:any=[];
  jobCards:any =[];
  jobCardarray:any =[];
  jobCardsearch:any =[];
  prodOrder:any = [];
  devisionTab:any=[];
  prodDevision:any=[];
  prodRoute:any=[];
  prodJobCosting:any=[];
  stockItems:any=[];
  salesId:any;
  OrderDate:string;
  DueDate:any;
  PlanProductionDate:any;
  SalesLine:string;
  StockCode:string;
  Note:string;
  isChecked:string;
  mySelect = '2';
  selectedValue: any;
  allDevision: any=[];
  selectedDevision:any=[];
  devisionTabDevision:any=[];
  userLoginData:any =[];
  OrderQTY:string;
  prodStatus:string;
  selectDevision:any=[];
  errorMessage: any;
  selectedLevel;
  carddata;
  unitCost:string;
  radioFilter:String;
  drawing:any=[];
  finishing:any=[];
  colourArray:any=[];
  PeriodicElement:any=[];
  jobCardElements:any=[];
  PeriodicElement1:any=[];
  assignTo :string
  jsonArr = [];
  devisionTabTable1 = [];
  devisionTabTable2 = [];
  selectedTableId:any;
  recRowId: Guid;

    ELEMENT_DATA//: PeriodicElement[] //= [

  constructor(private http:HttpClient, private modalService: NgbModal) {
    this.isChecked = 'false';
    this.dataSource = this.ELEMENT_DATA;
    this.displayedColumns =  ['jobcard','salesid', 'stockcode', 'status', 'instage','orderdate','DueDate','CompleteDate','OrderQTY','ProductionStartDate','ProductionComplete','Sales','Cost','Gp','Note','AddtionalNotes','Color'];
    //Get All User
    this.getAllUser().subscribe(data => {
      this.userLoginData = data;
      });

      this.getStockItem().subscribe(data => {
        this.stockItems = data;
      })

      this.getProdJobCosting().subscribe(data => {
        this.prodJobCosting = data;
      })

      this.getProdroute().subscribe(data => {
        this.prodRoute = data;
      })


   }

   TableComponent(){
     console.log('=====================================Constructor=========================')
   }

  ngOnInit() {
   console.log('==============================================InIt=============================')
    this.PeriodicElement.length = 0;
    this.jobCardElements.length = 0;
    this.getAllJobCards().subscribe(data => {
      this.jobCardElements = data;
       for (var i = 0; i < data.length; i++){
        var rowColour;
         console.log('Due Date =>>>>>>>>>>>>> '+new Date(data[i]['DueDate']).toLocaleDateString('en-CA'))
         
         var d = new Date();
          d.setDate(d.getDate()-1);
          
         if(new Date(data[i]['DueDate']).toLocaleDateString('en-CA') < new Date().toLocaleDateString('en-CA')){
          rowColour = 'RED'
         }
         if(new Date(data[i]['DueDate']).toLocaleDateString('en-CA') == d.toLocaleDateString('en-CA')){
          rowColour = 'YELLO'
         }
         if(new Date(data[i]['DueDate']).toLocaleDateString('en-CA') > new Date().toLocaleDateString('en-CA')){
          rowColour = 'GREEN'
         }

         console.log('Colour ========== '+ rowColour)
         this.PeriodicElement.push({
          jobcard:data[i]['SalesId'],
          salesid:data[i]['SalesId'],
          stockcode:data[i]['StockCode'],
          status: data[i]['Status'],
          instage: data[i]['InStage'],
          orderdate: new Date(data[i]['OrderDate']).toLocaleDateString('en-CA'),
          DueDate: new Date(data[i]['DueDate']).toLocaleDateString('en-CA'),
          CompleteDate: new Date(data[i]['DueDate']).toLocaleDateString('en-CA'),
          OrderQTY:data[i]['OrderQTY'],
          ProductionStartDate:new Date(data[i]['ProductionStartDate']).toLocaleDateString('en-CA'),
          ProductionComplete:new Date(data[i]['ProductionComplete']).toLocaleDateString('en-CA'),
          Sales:data[i]['Sales'],
          Cost:data[i]['Cost'],
          Gp:data[i]['Gp'],
          Note:data[i]['Note'],
          AddtionalNotes:data[i]['AddtionalNotes'],
          Color:data[i]['Color'],
          rowColour:rowColour




         })
         this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
         this.dataSource.sort = this.sort;
         this.dataSource.paginator = this.paginator;
         }
      });


      this.drawing = [
        {id: 0, name: "None"},
        {id: 1, name: "Custom Drawing"},
        {id: 2, name: "Stock Drawing"},
        {id: 3, name: "N/A"}
    ];

    this.finishing =[
        {id: 0, name: "Pickel"},
        {id: 1, name: "Paint Welds Only"},
        {id: 2, name: "Polish Welds"},
        {id: 3, name: "N/A"}
    ];

    this.colourArray =[
      {id: 0, name: "No Paint"},
      {id: 1, name: "Silver"},
      {id: 2, name: "Fint Gray"},
      {id: 3, name: "Matt Black"},
      {id: 4, name: "N/A"}
    ]
}



  deleteCurrentUser(user):void{

    if(localStorage.getItem('Users')){
      for (var i = 0; i < JSON.parse(localStorage.getItem('Users')).length; i++){
        if(JSON.parse(localStorage.getItem('Users'))[i].userFirstName === user.userFirstName){
         this.userData.remove(JSON.parse(localStorage.getItem('Users'))[i]);

        }
      }
    localStorage.getItem('Users')
  }
}

  //Call to Api and get data.
  getAllJobCards():Observable<String[]>{
    this.jobCardElements = this.http.get<String[]>('http://localhost:50388/api/jobcard');
    return this.jobCardElements;
  }

  viewSalesId(salesId):void{
  }

  Search(){
    if (this.salesId === undefined) return [];
      this.jobCards = this.jobCards.filter(res => {
        return res.Note.toLocaleLowerCase().match(this.salesId.toLocaleLowerCase());
      });
    
  }

  viewJobCard(id,content){
    this.jobCards = this.jobCards.filter(res => {
      if(res.SalesId === id){
        this.prodOrder.push(res)
        this.modalService.open(content, { size: 'xl' });
      }
    });
    
    

  }

  openJobCardDetails(content,id) {
    console.log('Button Click');
    this.allDevision.length = 0;
    this.prodDevision.length = 0;

    //Filter All Devision according to the selected Id
    this.getProdroute().subscribe(data => {
      for(var x = 0 ;x<data.length; x++){
        
        if(!data[x]['enable'] && data[x]['soSeqNo'] === id){
           this.prodDevision.push(data[x]);
        }
      }
      });


    this.getAllJobCards().subscribe(data => {

      for (var i = 0; i < data.length; i++){
        if(data[i]['SalesId'] === id){
          this.salesId = data[i]['SalesId'];
          this.OrderDate =new  Date(data[i]['OrderDate']).toLocaleDateString('en-CA');
          this.DueDate =  new Date( data[i]['DueDate']).toLocaleDateString('en-CA');
          this.PlanProductionDate = new Date( data[i]['PlanProductionDate']).toLocaleDateString('en-CA');
          this.Note =  data[i]['Note'];
          this.StockCode = data[i]['StockCode'];
          this.OrderQTY = data[i]['OrderQTY'];
          this.prodStatus = data[i]['Status'];
          this.prodOrder.length = 0;
          this.prodOrder.push(data[i]);
          this.modalService.open(content, { size: 'xl' });



        }

        

        }
      });

      this.selectDevision.length = 0;
      this.getProdroute().subscribe(data => {
        for (var i = 0; i < data.length; i++){
          
          if(data[i]['soSeqNo'] === id && data[i]['enable']){
            
            this.selectDevision.push(data[i]);
            


          }
         
        }
        });

    
  }
  

    //Call to Api and get data.
    getJobCardById(id):Observable<String[]>{
      return this.http.get<String[]>('http://localhost:50388/api/jobcard/get/3036');
    }

  openX2(content) {
    this.jobCards = this.jobCards.filter(res => {
      if(res.SalesId === 10017){
        this.Note = res.Note;
        this.SalesLine = res.SalesLine;
        this.StockCode = res.StockCode;
        this.prodOrder.push(res)
        //this.modalService.open(content, { size: 'xl' });
      }
    });
   // this.modalService.open(content, { size: 'xl' });
  }

  savePopupContent(){
  }


  AddItem(){
    for(var x = 0;x<this.devisionTabTable1.length;x++){
      if(this.selectedTableId == this.devisionTabTable1[x]['Id']){
        this.recRowId = this.devisionTabTable1[x]['Id'];
      }
    }

    console.log('ID is2 => '+this.recRowId)
    this.devisionTabTable2.push({StockCode: 'foo', Id: 'this.recRowId' , Note: '<td><input type="text" id="fname" name="fname"></td>'});



  }

  myFunction() {
    var x = document.getElementById("mySelect");
    x.remove();
  }
  


  filterChanged(selectedValue:string){
 
    }

    selectChange(mySelect) {
  }

  onSubmit(Form :NgForm){
  }
  myFunc(num1) {
    var num = ((document.getElementById("exchageRateDate") as HTMLInputElement).value);
  }



  moveLeftSelected(){
    var selectvalue;
    for(var x=0;x<this.selectedDevision.length;x++)
    {
      selectvalue = this.selectedDevision[x]
    }
    console.log('selectvalue : '+selectvalue)

    for(var x = 0;x< this.selectDevision.length; x++){
      console.log('Prod '+this.selectDevision[x]['opCode'])
      if(this.selectDevision[x]['opCode'] === selectvalue ){
        this.selectDevision[x]['enable'] = false;
        console.log('selectvalue456 : '+selectvalue)
        this.prodDevision.push( this.selectDevision[x]);
        this.selectDevision.splice(x, 1);
        

      }

    }

    var url = 'http://localhost:50388/api/prodroute/addprodRoute';
    const body=JSON.stringify(this.prodDevision);
  let headers = new Headers({ 'Content-Type': 'application/json' });
  this.http.post(url, this.prodDevision).subscribe(res => console.log(res));

  }

  moveRightSelected(){
    console.log('Button '+this.allDevision.length)
    console.log('Button '+this.prodDevision.length)
    var selectvalue;
    for(var x=0;x<this.allDevision.length;x++)
    {
      selectvalue = this.allDevision[x]
    }
    console.log('selectvalue : '+selectvalue)

    for(var x = 0;x< this.prodDevision.length; x++){
      console.log('Prod '+this.prodDevision[x]['opCode'])
      if(this.prodDevision[x]['opCode'] === selectvalue ){
        this.prodDevision[x]['enable'] = true;
        console.log('selectvalue456 : '+selectvalue)
        this.prodDevision[x].remove;
        this.selectDevision.push( this.prodDevision[x]);
        this.prodDevision.splice(x, 1);


        

      }

    }
    var url = 'http://localhost:50388/api/prodroute/addprodRoute';
    const body=JSON.stringify(this.selectDevision);
  let headers = new Headers({ 'Content-Type': 'application/json' });
  this.http.post(url, this.selectDevision).subscribe(res => console.log(res));
  }

  SaveItem(selectdata){
    this.devisionTabDevision.length = 0;
    this.devisionTabTable1.length = 0;
    this.prodRoute.length =0;
    for(var x=0;x<this.selectDevision.length;x++)
    {
      this.devisionTabDevision.push(this.selectDevision[x]['opCode']);
     
    }

    // for(var x=0;x<this.allDevision.length;x++)
    // {
    //   this.devisionTabDevision.push(this.allDevision[x]);
     
    // }


    this.getProdroute().subscribe(data => {
      this.prodRoute = data;
      for (var i = 0; i < data.length; i++){
        if(this.devisionTabDevision.includes(data[i]['opCode']) && 
        data[i]['soSeqNo'] == this.salesId){
          this.devisionTabTable1.push({
            Id: data[i]['id'],
            StockCode: data[i]['stockCode'],
            Devision:data[i]['opCode'],
            AssignTo:data[i]['assignTo'],
            OrderQTY:data[i]['orderQty'],
            EstimatedTime:data[i]['estimatedTime'],
            Status:data[i]['status']
        });
        
        }
       
      }
      });

    for(var x = 0;x<selectdata.length;x++){
      this.jobCardarray.push({
        Drawing:selectdata[x]['Drawing'],
        Division:selectdata[x]['Division'],
        Color:selectdata[x]['Color'],
        Note :selectdata[x]['Note'],
        AddtionalNotes :selectdata[x]['AddtionalNotes'],
        Id :selectdata[x]['Id'],

      })


      console.log('Drawing '+selectdata[x]['Drawing'])
      console.log('Division '+selectdata[x]['Division'])
      console.log('Color '+selectdata[x]['Color'])
      console.log('Note '+selectdata[x]['Note'])
      console.log('AddtionalNotes '+selectdata[x]['AddtionalNotes'])
      console.log('Item '+((document.getElementById("exampleCheck1") as HTMLInputElement).checked))

      var url = 'http://localhost:50388/api/JobCard/addcard';
  const body=JSON.stringify(this.jobCardarray);
  let headers = new Headers({ 'Content-Type': 'application/json' });
this.http.post(url, this.jobCardarray).subscribe(res => console.log(res));

    }

    //  var txtDrawing = document.getElementById("orderData.Drawing").select as HTMLInputElement).selet;

    


  }

  getAllUser():Observable<String[]>{
    this.userLoginData = this.http.get<String[]>('http://localhost:50388/api/staff');
    return this.userLoginData;
  }

  getAllDevision():Observable<String[]>{
    this.prodDevision = this.http.get<String[]>('http://localhost:50388/api/Devision');
    return this.prodDevision;
  }

  getProdroute():Observable<String[]>{
  this.prodRoute = this.http.get<String[]>('http://localhost:50388/api/prodroute');
    return this.prodRoute;
  }

  getStockItem():Observable<String[]>{
    this.stockItems = this.http.get<String[]>('http://localhost:50388/api/StockItem');
      return this.stockItems;
    }

    getProdJobCosting():Observable<String[]>{
      this.prodJobCosting = this.http.get<String[]>('http://localhost:50388/api/prodjobcosting');
        return this.prodJobCosting;
      }

  moveRight(){
    
    for(var x =0;x<this.prodDevision.length;x++ ){
      for(var j =0; j< this.allDevision.length;j++){
        if(this.prodDevision[x]['id'] == this.allDevision[j]){
          this.allDevision.remove(2);
        }
      }
    }

  }
  end:string;

  endModel(modelStarus){

  }
  
  startmodel(modelStarus){
   for(var x = 0;x<this.devisionTabTable1.length;x++){
     if(this.selectedTableId == this.devisionTabTable1[x]['Id']){
       if(this.devisionTabTable1[x]['Status'] != 'Created' && 
       this.devisionTabTable1[x]['Status'] != 'Start'){
        var url = null;
         if(modelStarus === 'Start'){
          this.devisionTabTable1[x]['Status'] = 'Start'
           url = 'http://localhost:50388/api/ProdRoute/updateroutestartstatus/'+this.selectedTableId ;

         }else{
          this.devisionTabTable1[x]['Status'] = 'End'
          url = 'http://localhost:50388/api/ProdRoute/updaterouteendstatus/'+this.selectedTableId ;

         }
        // this.prodRoute = this.http.post<any>(url,'Save');



        this.http.post<any>(url, { title: 'Angular POST Request Example' }).subscribe({
          next: data => {
              console.log(data.id) ;
          },
          error: error => {
              this.errorMessage = error.message;
              console.error('There was an error!', error);
          }
      })


       // return this.prodRoute;
         
       }else{
        alertify.error("That Status not should be Created or Start");

       }

     }


   }

  }


  onSelectRadioBtn(selectedItem: any) {
    this.selectedTableId = selectedItem.Id;
    console.log('Select Id '+this.selectedTableId)
    this.devisionTabTable2.length = 0;
    for(var j=0;j<this.prodJobCosting.length;j++)
    {
      if(this.prodJobCosting[j]['refUID'] == this.selectedTableId ){
        this.prodJobCosting[j]['transDate'] = new Date(this.prodJobCosting[j]['transDate']).toLocaleDateString('en-CA');
        this.devisionTabTable2.push(this.prodJobCosting[j]);

      }

    }
}

onSelect1(selectedItem: any) {
  selectedItem.Devision = 'Abc';
}

onOptionsSelected(dropdownValue: any){
  var num = ((document.getElementById("comboItemCode") as HTMLInputElement).value);

}

data:Array<Object> = [
    {id: 0, name: "name1"},
    {id: 1, name: "name2"}
];

//When 
selectItemCode(selectedItem: any){
  var today = new Date();
  for(var x = 0;x<this.stockItems.length;x++)
  {
    if(this.stockItems[x]['stockCode'] == selectedItem.itemCode){


      selectedItem.description = this.stockItems[x]['description']
      selectedItem.unitCost = this.stockItems[x]['latestCost']
      selectedItem.avgCost = this.stockItems[x]['aveCost']
      // selectedItem.qty = this.stockItems[x]['qty']
      selectedItem.transDate = today.toLocaleDateString('en-CA')
      // selectedItem.transDate = new Date( this.stockItems[x]['transDate']).toLocaleDateString('en-CA');
    }

    

  }
}

changeQuantity(selectedItem: any){
  var num = ((document.getElementById("txtQuantity") as HTMLInputElement).value);

}
onQuantityChange(searchValue: string,carddata:any): void {  
  
  var lineAmount = parseFloat(carddata.unitCost);
  carddata.lineAmount = lineAmount * parseFloat(searchValue);

}



//Save Devision Button
btnSaveDevision(data)
{


  for(var x = 0;x<this.devisionTabTable1.length;x++){
    if(this.selectedTableId == this.devisionTabTable1[x]['Id']){
      console.log('ID is => '+this.devisionTabTable1[x]['Id'])
       this.recRowId =this.devisionTabTable1[x]['Id'];
    }
  }

  console.log('this.recRowId => '+this.recRowId)

  this.jobCostingArray.length = 0;
  for(var x =0;x<data.length; x++){
    this.jobCostingArray.push({
      recId:data[x]['recId'],
      RefUID:this.recRowId,
      itemCode:data[x]['itemCode'],
      unitCost:parseFloat(data[x]['unitCost']),
      lineAmount:parseFloat(data[x]['lineAmount']),
      avgCost:parseFloat(data[x]['avgCost']),
      latestCost:parseFloat(data[x]['latestCost']),
      description:data[x]['description'],
      transDate:data[x]['transDate'],
      qty:parseFloat(data[x]['qty']),
    });
  }
  var url = 'http://localhost:50388/api/prodjobcosting/addProdJob';
  const body=JSON.stringify(this.jobCostingArray);
  let headers = new Headers({ 'Content-Type': 'application/json' });
this.http.post(url, this.jobCostingArray).subscribe(res => console.log(res));
}

  extractData(extractData: any) {
    throw new Error('Method not implemented.');
  }

  searchModel(){
    //jobCards
    //console.log('Searching...')
    var x =((document.getElementById("radioSalesOrder") as HTMLInputElement).checked);

    //this.jobCardsearch.length = 0;
    this.PeriodicElement1.length = 0;

    if((document.getElementById("radioSalesOrder") as HTMLInputElement).checked){
      var salesOrderId =((document.getElementById("txtSalesOrder") as HTMLInputElement).value);
       for(var y = 0; y<this.PeriodicElement.length;y++){
         
        
         if(this.PeriodicElement[y]['salesid'] == salesOrderId){
          console.log("Search for loop "+ this.PeriodicElement[y]['salesid'] +" - "+y);
    //       console.log('Search1111...' + salesOrderId);
    //       console.log('Search ID '+this.PeriodicElement[y]['salesid']+ " "+salesOrderId)
    //       //this.PeriodicElement.length = 0;
    //       this.jobCardsearch.push(this.PeriodicElement[y]);

    this.PeriodicElement1.push(this.PeriodicElement[y]);
    this.dataSource = new MatTableDataSource(this.PeriodicElement1) ;
         
       }

     }
    if(this.jobCardsearch.length > 0){
      this.PeriodicElement.length = 0;
      this.PeriodicElement = this.jobCardsearch;
      this.dataSource = new MatTableDataSource(this.PeriodicElement) ;

    }

  }if((document.getElementById("radioDueDate") as HTMLInputElement).checked){

    var fromDate =((document.getElementById("fromDate") as HTMLInputElement).value);
    var toDate =((document.getElementById("toDate") as HTMLInputElement).value);


    
    for(var y = 0; y<this.PeriodicElement.length;y++){
      var dbfromDate = new Date(this.PeriodicElement[y]['DueDate']).toLocaleDateString('en-CA');
      var dbToDate = new Date(this.PeriodicElement[y]['DueDate']).toLocaleDateString('en-CA');

      if(dbfromDate > fromDate  && dbToDate < toDate){
        // this.jobCardsearch.push(this.PeriodicElement[y]);
        this.PeriodicElement1.push(this.PeriodicElement[y]);
    this.dataSource = new MatTableDataSource(this.PeriodicElement1) ;
    }
    }

    if(this.jobCardsearch.length > 0){
      this.PeriodicElement.length = 0;
      this.PeriodicElement = this.jobCardsearch;
      this.dataSource = new MatTableDataSource(this.PeriodicElement) ;

    }
    // this.jobCards.length = 0;
    //   this.jobCards = this.jobCardsearch;
    //   console.log('Search '+this.jobCardsearch.length)



  }
  }

  radioDueDate(){
    console.log('Radio...');
  }
  radioSalesOrder(){
    console.log('Sales...');
  }

  resetSearchModel(){
    //this.ngOnInit();
    // this.getAllJobCards().subscribe(data => {
    //   for (var i = 0; i < data.length; i++){
    //     data[i]['OrderDate'] = new Date(data[i]['OrderDate']).toLocaleDateString('en-CA'); 
    //      data[i]['DueDate'] = new Date(data[i]['DueDate']).toLocaleDateString('en-CA'); 
    //      data[i]['DueDate'] = new Date(data[i]['DueDate']).toLocaleDateString('en-CA'); 
    //      data[i]['ProductionStartDate'] = new Date(data[i]['ProductionStartDate']).toLocaleDateString('en-CA'); 
    //     this.jobCards = data;
    //     }
    //   });

    this.PeriodicElement.length = 0;

    this.getAllJobCards().subscribe(data => {
      for (var i = 0; i < data.length; i++){
        this.PeriodicElement.push({
         jobcard:data[i]['SalesId'],
         salesid:data[i]['SalesId'],
         stockcode:data[i]['StockCode'],
         status: data[i]['Status'],
         instage: data[i]['InStage'],
         orderdate: new Date(data[i]['OrderDate']).toLocaleDateString('en-CA'),
         DueDate: new Date(data[i]['DueDate']).toLocaleDateString('en-CA'),
         CompleteDate: new Date(data[i]['DueDate']).toLocaleDateString('en-CA'),
         OrderQTY:data[i]['OrderQTY'],
         ProductionStartDate:new Date(data[i]['ProductionStartDate']).toLocaleDateString('en-CA'),
         ProductionComplete:new Date(data[i]['ProductionComplete']).toLocaleDateString('en-CA'),
         Sales:data[i]['Sales'],
         Cost:data[i]['Cost'],
         Gp:data[i]['Gp'],
         Note:data[i]['Note'],
         AddtionalNotes:data[i]['AddtionalNotes'],
         Color:data[i]['Color'],
        })
        this.dataSource = new MatTableDataSource(this.PeriodicElement) ;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        }
     });

  }

  advancedSearchModel(){
    var txtSalesId = (document.getElementById("txtSalesId") as HTMLInputElement).value;
    var txtStockCode = (document.getElementById("txtStockCode") as HTMLInputElement).value;
    var txtDueDate = (document.getElementById("txtDueDate") as HTMLInputElement).value;
    var OrderDate = (document.getElementById("OrderDate") as HTMLInputElement).value;
    var ProductionStartDate = (document.getElementById("ProductionStartDate") as HTMLInputElement).value;

    
    if(txtSalesId != ''){
      
      var sign = (document.getElementById("cbSalesId") as HTMLInputElement).value;
      this.checkSalesId(sign,txtSalesId);
    }
    if(txtStockCode != ''){
      var sign = (document.getElementById("cbStockCode") as HTMLInputElement).value;
      this.checkStockCode(sign,txtStockCode);
    }

    if(txtDueDate != ''){
      var sign = (document.getElementById("cbStockCode") as HTMLInputElement).value;
      this.checkDueDate(sign,txtDueDate);
    }

    if(OrderDate != ''){
      var sign = (document.getElementById("cbOrderDate") as HTMLInputElement).value;
      this.checkOrderDate(sign,txtDueDate);
    }

    if(ProductionStartDate != ''){
      var sign = (document.getElementById("cbProdStartDate") as HTMLInputElement).value;
      this.checkProductionStartDate(sign,txtDueDate);
    }


  }

  checkProductionStartDate(sign,value){
    this.jobCardsearch.length = 0;
    if(sign == 'like'){
      for(var y = 0; y<this.jobCards.length;y++){
        var str = this.jobCards[y]['ProductionStartDate'];
        if(str == value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '='){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['ProductionStartDate'] == value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '<>'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['ProductionStartDate'] != ''){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '<'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['ProductionStartDate'] < value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '>'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['ProductionStartDate'] > value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }
    this.jobCards = this.jobCardsearch;
  }



  checkOrderDate(sign,value){
    this.jobCardsearch.length = 0;
    if(sign == 'like'){
      for(var y = 0; y<this.jobCards.length;y++){
        var str = this.jobCards[y]['OrderDate'];
        if(str == value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '='){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['OrderDate'] == value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '<>'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['OrderDate'] != ''){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '<'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['OrderDate'] < value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '>'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['OrderDate'] > value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }
    this.jobCards = this.jobCardsearch;
  }



  checkDueDate(sign,value){
    this.jobCardsearch.length = 0;
    if(sign == 'like'){
      for(var y = 0; y<this.jobCards.length;y++){
        var str = this.jobCards[y]['DueDate'];
        if(str == value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '='){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['DueDate'] == value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '<>'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['DueDate'] != ''){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '<'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['DueDate'] < value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '>'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['DueDate'] > value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }
    this.jobCards = this.jobCardsearch;
  }

  checkStockCode(sign,value){
    this.jobCardsearch.length = 0;
    if(sign == 'like'){
      for(var y = 0; y<this.jobCards.length;y++){
        var str = this.jobCards[y]['StockCode'];
        if(str == value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '='){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['StockCode'] == value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '<>'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['StockCode'] != ''){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '<'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['StockCode'] < value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '>'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['StockCode'] > value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }
    this.jobCards = this.jobCardsearch;


  }

  applyFilter(filterValue:string){
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  printjobcard(element){

    const doc = new jsPDF();
    var width = doc.internal.pageSize.getWidth();
var height = doc.internal.pageSize.getHeight();
var myAssign = '';

for (var i = 0; i < this.prodRoute.length; i++){
   var t1 = '610/650C/K200 UPGRADE'
   doc.setTextColor('#000000');
  var opcode = String(this.prodRoute[i]['opCode'])
  var instage =  String(element.instage)
  if(this.prodRoute[i]['soSeqNo'] === element.salesid && trim(opcode) === trim(instage)){
     this.assignTo = String(this.prodRoute[i]['assignTo'])
  }
}

doc.setFont("helvetica");
doc.setFontSize(20);
doc.setLineWidth(2);
doc.text(''+element.salesid, 160, 10);

var logoimg = new Image()
logoimg.src = 'assets/images/logo.png'
doc.addImage(logoimg, 'png', 20, 7, 20, 12)

var barCodeimg = new Image()
barCodeimg.src = 'assets/images/barcode.png'
doc.addImage(barCodeimg, 'png', 160, 12, 20, 8)


doc.setFontSize(13);
doc.setLineWidth(2);
doc.text(''+element.instage, 80 , 15);

doc.setFontSize(17);
doc.setLineWidth(0.3);
doc.rect(20, 25, 85, 25);
doc.setFontSize(10);
doc.setLineWidth(0.3);
doc.text('TERRY\'S SPARES', 35 , 30);
doc.text('Phone No: 03-7461-3843', 35 , 45);
doc.rect(20, 25, 170, 25);

doc.setFontSize(10);
doc.setLineWidth(0.3);
doc.text('164 CENTRAL PARK DRIVE', 120 , 30);
doc.text('RICHMOND', 120 , 35);
doc.text('MELBOURNE', 120 , 40);
doc.text('AUSTRALIA', 120 , 45);

doc.setFontSize(10);
doc.setLineWidth(0.3);
doc.setTextColor('#FFFFFF');
doc.rect(20, 50, 170, 7,'FD');
doc.text('Customer Note :'+element.Note, 22 , 54);

doc.rect(20, 57, 170, 10);
doc.rect(20, 57, 40, 10);
doc.rect(60, 57, 30, 10);
doc.rect(90, 57, 30, 10);
doc.rect(120, 57, 30, 10);

doc.setFontSize(7);
doc.setLineWidth(0.3);
doc.setTextColor('#000000');

doc.text('Customer Order No', 22 , 60);
doc.text('Shipped By', 62 , 60);
doc.text('Order Date', 92 , 60);
doc.text(''+element.orderdate, 92 , 64);
doc.text('Due Date', 122 , 60);
doc.text(''+element.DueDate, 122 , 64);
doc.text('Entered By', 152 , 60);
doc.text(''+this.assignTo, 152 , 64);

doc.setFontSize(10);
doc.setLineWidth(0.3);
doc.setTextColor('#000000');
doc.setFillColor('#C6CED8'); 
doc.rect(20, 67, 35, 8,'FD');
doc.rect(55, 67, 100, 8,'FD');
doc.rect(155, 67, 35, 8,'FD');
doc.text('ITEM CODE', 22 , 72);
doc.text('DESCRIPTION', 100 , 72);
doc.text('QTY ORDERED', 157 , 72);
doc.rect(20, 77, 35, 8);
doc.rect(55, 77, 100, 8);
doc.rect(155, 77, 35, 8)

doc.setFontSize(7);
doc.setLineWidth(0.3);
for(var x=0;x<this.stockItems.length;x++){
  if(this.stockItems[x]['stockCode'] === element.stockcode){
    doc.text(''+element.stockcode, 22 , 81);
    doc.text(''+this.stockItems[x]['description'], 58 , 81);
    doc.text(''+element.OrderQTY, 157 , 81);
  }
}

doc.setFillColor('#C6CED8');
doc.rect(20, 85, 35, 8);
doc.rect(20, 93, 35, 8,'FD');
doc.rect(55, 85, 60, 16,'FD');
doc.rect(115, 85, 75, 8,'FD');
doc.rect(115, 93, 75, 8,'FD')
doc.rect(20, 101, 170, 8)



for(var x =0;x<this.jobCardElements.length;x++){
if(this.jobCardElements[x]['SalesId'] === element.salesid ){
  doc.text('DRAWING:'+this.jobCardElements[x]['Drawing'], 57 , 90);
  doc.text('FINISH:'+this.jobCardElements[x]['Division'], 57 , 94);
  doc.text('COLOUR:'+this.jobCardElements[x]['Color'], 57 , 98);
}
}

console.log(this.prodRoute.length)



 
  
  doc.text('ASSIGN TO : '+this.assignTo, 118 , 88);
  doc.text('EST.COMPLETION DATE : '+element.CompleteDate, 118 , 92);
  doc.text('FINISHED DATE & TIME : '+element.CompleteDate, 118 , 97);
  doc.text('BIN LOCATION : ', 21 , 97);
  doc.text('Note :'+element.Note, 21 , 105);






//doc.setTextColor(255, 0, 0);

doc.save("a4.pdf");
    
  }




  checkSalesId(sign,value){
    this.jobCardsearch.length = 0;
    if(sign == 'like'){
      for(var y = 0; y<this.jobCards.length;y++){
        var str = this.jobCards[y]['SalesId'];
        if(str == value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '='){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['SalesId'] == value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '<>'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['SalesId'] != ''){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '<'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['SalesId'] < value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }

    if(sign == '>'){
      for(var y = 0; y<this.jobCards.length;y++){
        if(this.jobCards[y]['SalesId'] > value){
          this.jobCardsearch.push(this.jobCards[y]);
      }
      }
    }
    this.jobCards = this.jobCardsearch;


  }


 



}
